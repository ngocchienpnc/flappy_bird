import { _decorator, CCInteger, Collider2D, Component, Contact2DType, director, EventKeyboard, Input, input, IPhysics2DContact, KeyCode, log, Node } from 'cc';
const { ccclass, property } = _decorator;

import { Ground } from './Ground';
import { Results } from './Results';
import { Bird } from './Bird';
import { PipePool } from './PipePool';
import { BirdAudio } from './BirdAudio';

@ccclass('GameCtrl')
export class GameCtrl extends Component {

    @property({
        type: Ground,
        tooltip: 'This is ground'
    })
    public ground: Ground;

    @property({
        type: Results
    })
    public results: Results;

    @property({
        type: Bird
    })
    public bird: Bird;

    @property({
        type: PipePool
    })
    public pipeQueue: PipePool;

    @property({
        type: BirdAudio
    })
    public birdAudio: BirdAudio;

    @property({
        type: CCInteger,
    })
    public speed: number = 300;

    @property({
        type: CCInteger,
    })
    public pipeSpeed: number = 200;
    public isOver: boolean;

    onLoad() {

        this.initListeners();
        this.results.resetScore();
        this.isOver = true;
        director.pause();
    }

    initListeners() {
        input.on(Input.EventType.KEY_DOWN, this.onKeyDown, this);

        this.node.on(Node.EventType.TOUCH_START, () => {
            console.log('TOUCH_START');

            if(this.isOver == true){
                this.resetGame()
                this.bird.resetBird()
                this.startGame()
            }
            if(this.isOver == false){
                this.bird.fly();
                this.birdAudio.onAudioQueue(3);
            }
            
            
        })
    }

    onKeyDown(event: EventKeyboard) {
        switch (event.keyCode) {
            case KeyCode.KEY_A:
                this.gameOver();
                break;
            case KeyCode.KEY_P:
                this.results.addScore();
                break;
            case KeyCode.KEY_Q:
                this.resetGame();
                this.bird.resetBird();
        }
    }


    startGame() {
        this.results.hideResults();
        director.resume();
    }

    gameOver() {
        this.results.showResults();
        this.isOver = true;
        this.birdAudio.onAudioQueue(0);
        director.pause();
    }

    resetGame() {
        this.results.resetScore();
        this.pipeQueue.reset();
        this.isOver = false;
        this.startGame();
    }

    passPipe(){
        this.results.addScore();
        this.birdAudio.onAudioQueue(2);
    }

    createPipe(){
        this.pipeQueue.addPool();
    }

    contactGroundPipe() {
    
        let collider = this.bird.getComponent(Collider2D);

        if (collider) {
          collider.on(Contact2DType.BEGIN_CONTACT, this.onBeginContact, this);
        }
    
      }

      onBeginContact(selfCollider: Collider2D, otherCollider: Collider2D, contact: IPhysics2DContact | null) {
        
        this.bird.hitSomething = true;
        this.birdAudio.onAudioQueue(1);
    
      }

    birdStruck(){
        this.contactGroundPipe();
        
        if(this.bird.hitSomething == true){
            this.gameOver();
        }
    }

     update(){
        if(this.isOver == false){
            this.birdStruck();
        }
     }
}


